import os
path = '/home/darya/Документы'

# просмотр текущей папки
print("Текущая папка:", os.getcwd())

#переход в другую папку
os.chdir(path)

#создание пустого файла
open(path, 'a').close()

#вывод содержимого файла
f = open('test.txt')
for line in f.readlines():
    print (line),

#вывод списка файлов в папке
for root, dirs, files in os.walk("."):
    for filename in files:
        print(filename)
