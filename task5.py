import requests

url = 'http://ip-api.com/json/'
ip = input('Get county by your IP: ')
response = requests.get(url + ip)
data = response.json()

if data['status'] == 'success':
    print('Your IP is located in', data['country'])
else:
    print("This IP doesn't exist")
