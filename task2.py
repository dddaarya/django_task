list = input().split()

try:
    print(*(x for x in list if not int(x) % 2))
except ValueError:
    print('элемент не является числом')


try:
    print(*(x for x in list if int(x) % 2))
except ValueError:
    print('элемент не является числом')


try:
    print(*(x for x in list if int(x) < 0))
except ValueError:
    print('элемент не является числом')
