import requests

url = ['http://s1.oboiki.net/uploads/images/others/2015/10/2f76354ffdfe3130c3efd1366353df15/varkraft-anduin-lotar_1920x1080.jpg',
       'http://s1.oboiki.net/uploads/images/others/2015/10/f566d6b2f6d1e13e3ef4b5b2f1d3e3fa/kotyara-morda-v-blizi_1920x1080.jpg',
       'http://s1.oboiki.net/uploads/images/others/2015/06/c2258ec79a3fab83b18426cbbc813feb/minony_1920x1080.jpg']

def get_file(url): #получаем изображание с сайте
    response = requests.get(url, stream=True)
    return response

def save_data(name, file_data): #сохраняем изображение на комп
    file = open(name, 'bw')
    for chunk in file_data.iter_content(4096): #4кб в файл
        file.write(chunk)

def get_name(url): #имя файла
    name = url.split('/')[-1]
    return name

for name in url:
    save_data(get_name(name), get_file(name))
