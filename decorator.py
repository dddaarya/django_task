from functools import wraps


def logthis(fnc):
    @wraps(fnc)
    def wrapped(*args, **kwargs):
        print('начальное значение:', args[0])
        res = fnc(*args, **kwargs)
        print('результат:', res)
        return res

    return wrapped


@logthis
def your_func(your_arg):
    return your_arg + 10


print('ваш результат:', your_func(10))
